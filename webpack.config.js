'use strict'

const weback = require('webpack');
const HOST = process.env.HOST || '127.0.0.1';
const PORT = process.env.PORT || '8888';

module.exports = {
  entry: [
		`webpack-dev-server/client?http://${HOST}:${PORT}`,
    `babel-polyfill`,
		`./app/index.js`
	],
  output: {
    path: './public',
		filename: 'bundle.js'
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loaders: [
          'style',
          'css?sourceMap',
          'sass'
        ]
      },
      {
        test: /\.png$/,
        loader: "url-loader?limit=100000000"
      },
      {
        test: /\.js?$/,
        exclude: /(node_modules|public)/,
        loader: 'babel',
        query: {
          presets: ['es2015', 'stage-0'],
        }
      },
      {
        test: /\.html$/,
        exclude: /(node_modules|public)/,
        loader: 'raw'
      }
    ]
  },
	devServer: {
		contentBase: "./public",
		noInfo: true,
		inline: true,
		historyApiFallback: true,
		port: PORT,
		host: HOST
	}
};
