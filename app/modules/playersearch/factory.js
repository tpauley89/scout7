
PlayerSearchFactory.$inject = ['$q', '$http'];

export default function PlayerSearchFactory($q, $http) {
  const factory = this;

  return {
    getCache: function() {
      let cachedPlayers = window.localStorage.scout7;
      return cachedPlayers !== undefined ? JSON.parse(cachedPlayers) : false;
    },
    setCache:  function(players) {
      window.localStorage.scout7 = JSON.stringify(players);
      return true;
    },
    getPlayers:  function() {
      // create promise
      var deffered = $q.defer();
      // get cache, returns players or false
      let cache = this.getCache();
      // cache not empty resolve with that data
      if (cache !== false) {
        deffered.resolve(cache);
      } else {
        // cache empty, make a call to the api, return data in promise
        $http.get('http://127.0.0.1:8889/api/players').then((response) => {
          this.setCache(response.data);
          // pass back data in promise
          deffered.resolve(response.data);
        }).catch(function(e){
          // pass back empty array in promise
          deffered.resolve([]);
          // log error
          console.log(e);
        });
      }

      // return promise
      return deffered.promise;
    }
  }
};
