Routes.$inject = ['$stateProvider'];

export default function Routes($stateProvider) {
  $stateProvider.
    state('playersearch', {
      url: '/',
      controller: 'PlayerSearchCtrl',
      controllerAs: 'ps',
      template: require('./index.html'),
      resolve: {
        players : function(PlayerSearchFactory){
          return PlayerSearchFactory.getPlayers();
        }
      }
    });
};
