import 'angular';

import Routes from './routes';
import PlayerSearchFactory from './factory';
import { PlayerSearchCtrl } from './controller';

export const PlayerSearch = angular
.module('scount7.PlayerSearch', [])
.config(Routes)
.factory('PlayerSearchFactory', PlayerSearchFactory)
.controller('PlayerSearchCtrl', PlayerSearchCtrl)
.name;
