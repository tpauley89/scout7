export const PlayerSearchCtrl = function(PlayerSearchFactory, players){
  const ctrl = this;
  // used by template to iterate through players
  ctrl.players = players;
  // used by template as filter sort parameter
  ctrl.filter = "";
  // used by template as orderBy sort parameter
  ctrl.orderBy = 'id';
  // used by template as reverse sort parameter
  ctrl.reverse = false;
  // used by modal to show/hide, used by deleteConfirmed as reference
  ctrl.delete = 0;
  // method called on click to change orderBy property
  ctrl.setOrderBy = (column) => {
    if (ctrl.orderBy === column) {
      ctrl.reverse = !ctrl.reverse;
      return;
    };
    ctrl.orderBy = column;
    ctrl.reverse = false;
    return;
  };
  // method called on click to bring up modal, set player to delete
  ctrl.setDelete = function(id) {
    ctrl.delete = id;
  };
  // method called by confirming player deletion
  ctrl.deleteConfirmed = function() {
    // filter players - return new array without player
    let updatedPlayers = ctrl.players.filter((player) => player.id === ctrl.delete ? false : true);
    // update view with new players list
    ctrl.players = updatedPlayers;
    // update cache with new list
    PlayerSearchFactory.setCache(updatedPlayers);
    // close modal
    ctrl.setDelete(0);
  };
};

PlayerSearchCtrl.$inject = ['PlayerSearchFactory', 'players'];
