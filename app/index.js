import 'angular';
import uiRouter from 'angular-ui-router';

import { PlayerSearch } from './modules/playersearch';
import './styles/style.scss';

angular
.module('scout7', [uiRouter, PlayerSearch])
.config(($locationProvider) => {
  // set up pretty urls - used in conjunction with base tag in /public/index.html head
  $locationProvider.html5Mode(true);
});
