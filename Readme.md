## Installation

Clone the repository, navigate to its root in a terminal window and run -

```bash
$ ./run.sh
```

In a second terminal (root also) run -

```bash
$ webpack-dev-server
```

Finally navigate to http://127.0.0.1:8888/

The steps above do the following - install yarn globally, install all yarn packages, start node express api. Then the webpack-dev-server commaned builds the client side app and serves it from memory.
