var express = require('express');
var app = express();

var router = express.Router();

router.get('/api/players', function(req, res) {
    res.json([
    {
      "id": 6,
      "name": "Paul Pogba",
      "position": "Central Midfield",
      "dateOfBirth": "1993-03-15",
      "nationality": "France",
      "contractUntil": "2021-06-30",
      "marketValue": "70,000,000 €"
    },
    {
      "id": 9,
      "name": "Zlatan Ibrahimovic",
      "position": "Centre Forward",
      "dateOfBirth": "1981-10-03",
      "nationality": "Sweden",
      "contractUntil": "2017-06-30",
      "marketValue": "12,000,000 €"
    },
    {
      "id": 3,
      "name": "Eric Bailly",
      "position": "Centre Back",
      "dateOfBirth": "1994-04-12",
      "nationality": "Cote d'Ivoire",
      "contractUntil": "2020-06-30",
      "marketValue": "20,000,000 €"
    },
    {
      "id": 22,
      "name": "Henrikh Mkhitaryan",
      "position": "Attacking Midfield",
      "dateOfBirth": "1989-01-21",
      "nationality": "Armenia",
      "contractUntil": "2020-06-30",
      "marketValue": "37,000,000 €"
    },
    {
      "id": 1,
      "name": "David de Gea",
      "position": "Keeper",
      "dateOfBirth": "1990-11-07",
      "nationality": "Spain",
      "contractUntil": "2019-06-30",
      "marketValue": "40,000,000 €"
    },
    {
      "id": 20,
      "name": "Sergio Romero",
      "position": "Keeper",
      "dateOfBirth": "1987-02-22",
      "nationality": "Argentina",
      "contractUntil": "2018-06-30",
      "marketValue": "5,000,000 €"
    },
    {
      "id": 32,
      "name": "Sam Johnstone",
      "position": "Keeper",
      "dateOfBirth": "1993-03-25",
      "nationality": "England",
      "contractUntil": "2017-06-30",
      "marketValue": "250,000 €"
    },
    {
      "id": 17,
      "name": "Daley Blind",
      "position": "Centre Back",
      "dateOfBirth": "1990-03-09",
      "nationality": "Netherlands",
      "contractUntil": "2018-06-30",
      "marketValue": "22,000,000 €"
    },
    {
      "id": 12,
      "name": "Chris Smalling",
      "position": "Centre Back",
      "dateOfBirth": "1989-11-22",
      "nationality": "England",
      "contractUntil": "2019-06-30",
      "marketValue": "20,000,000 €"
    },
    {
      "id": 5,
      "name": "Marcos Rojo",
      "position": "Centre Back",
      "dateOfBirth": "1990-03-20",
      "nationality": "Argentina",
      "contractUntil": "2019-06-30",
      "marketValue": "19,000,000 €"
    },
    {
      "id": 4,
      "name": "Phil Jones",
      "position": "Centre Back",
      "dateOfBirth": "1992-02-21",
      "nationality": "England",
      "contractUntil": "2019-06-30",
      "marketValue": "14,000,000 €"
    },
    {
      "id": 23,
      "name": "Luke Shaw",
      "position": "Left-Back",
      "dateOfBirth": "1995-07-12",
      "nationality": "England",
      "contractUntil": "2018-06-30",
      "marketValue": "21,000,000 €"
    },
    {
      "id": 36,
      "name": "Matteo Darmian",
      "position": "Right-Back",
      "dateOfBirth": "1989-12-02",
      "nationality": "Italy",
      "contractUntil": "2019-06-30",
      "marketValue": "15,000,000 €"
    },
    {
      "id": 25,
      "name": "Antonio Valencia",
      "position": "Right-Back",
      "dateOfBirth": "1985-08-04",
      "nationality": "Ecuador",
      "contractUntil": "2017-06-30",
      "marketValue": "8,000,000 €"
    },
    {
      "id": 28,
      "name": "Morgan Schneiderlin",
      "position": "Defensive Midfield",
      "dateOfBirth": "1989-11-08",
      "nationality": "France",
      "contractUntil": "2019-06-30",
      "marketValue": "28,000,000 €"
    },
    {
      "id": 16,
      "name": "Michael Carrick",
      "position": "Defensive Midfield",
      "dateOfBirth": "1981-07-28",
      "nationality": "England",
      "contractUntil": "2017-06-30",
      "marketValue": "3,000,000 €"
    },
    {
      "id": 24,
      "name": "Timothy Fosu-Mensah",
      "position": "Defensive Midfield",
      "dateOfBirth": "1998-01-02",
      "nationality": "Netherlands",
      "contractUntil": "2017-06-30",
      "marketValue": "1,000,000 €"
    },
    {
      "id": 21,
      "name": "Ander Herrera",
      "position": "Central Midfield",
      "dateOfBirth": "1989-08-14",
      "nationality": "Spain",
      "contractUntil": "2018-06-30",
      "marketValue": "28,000,000 €"
    },
    {
      "id": 27,
      "name": "Marouane Fellaini",
      "position": "Central Midfield",
      "dateOfBirth": "1987-11-22",
      "nationality": "Belgium",
      "contractUntil": "2018-06-30",
      "marketValue": "15,000,000 €"
    },
    {
      "id": 31,
      "name": "Bastian Schweinsteiger",
      "position": "Central Midfield",
      "dateOfBirth": "1984-08-01",
      "nationality": "Germany",
      "contractUntil": "2018-06-30",
      "marketValue": "10,000,000 €"
    },
    {
      "id": 8,
      "name": "Juan Mata",
      "position": "Attacking Midfield",
      "dateOfBirth": "1988-04-28",
      "nationality": "Spain",
      "contractUntil": "2018-06-30",
      "marketValue": "31,000,000 €"
    },
    {
      "id": 7,
      "name": "Memphis Depay",
      "position": "Left Wing",
      "dateOfBirth": "1994-02-13",
      "nationality": "Netherlands",
      "contractUntil": "2019-06-30",
      "marketValue": "25,000,000 €"
    },
    {
      "id": 18,
      "name": "Ashley Young",
      "position": "Left Wing",
      "dateOfBirth": "1985-07-09",
      "nationality": "England",
      "contractUntil": "2018-06-30",
      "marketValue": "10,000,000 €"
    },
    {
      "id": 14,
      "name": "Jesse Lingard",
      "position": "Left Wing",
      "dateOfBirth": "1992-12-15",
      "nationality": "England",
      "contractUntil": "2018-06-30",
      "marketValue": "6,000,000 €"
    },
    {
      "id": 10,
      "name": "Wayne Rooney",
      "position": "Secondary Striker",
      "dateOfBirth": "1985-10-24",
      "nationality": "England",
      "contractUntil": "2019-06-30",
      "marketValue": "35,000,000 €"
    },
    {
      "id": 11,
      "name": "Anthony Martial",
      "position": "Centre Forward",
      "dateOfBirth": "1995-12-05",
      "nationality": "France",
      "contractUntil": "2019-06-30",
      "marketValue": "32,000,000 €"
    },
    {
      "id": 19,
      "name": "Marcus Rashford",
      "position": "Centre Forward",
      "dateOfBirth": "1997-10-31",
      "nationality": "England",
      "contractUntil": "2020-06-30",
      "marketValue": "7,000,000 €"
    }
  ]);
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(router);
app.listen(8889);
